cmake_minimum_required(VERSION 3.18)
project(CudaTest)

enable_language(CUDA)

add_executable(cuda-test main.cpp saxpy.h saxpy.cu)

target_compile_options(cuda-test PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-Werror=return-type>)
target_compile_options(cuda-test PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:-lineinfo>)#todo: only in debug?
#target_compile_options(cuda-test PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:--expt-relaxed-constexpr)
target_compile_features(cuda-test PRIVATE cxx_std_17)
set_target_properties(
        cuda-test
        PROPERTIES
        CUDA_SEPARABLE_COMPILATION ON)
