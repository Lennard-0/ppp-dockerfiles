
#include "saxpy.h"
#include <iostream>

#define checkError(ans) { cudaAssert((ans), __FILE__, __LINE__); }
inline void cudaAssert(cudaError_t code, const char *file, int line)
{
  if (code != cudaSuccess)
  {
    std::cerr << "cuda error: " << cudaGetErrorString(code) << " in " << file << " at " << line << "\n";
    exit(code);
  }
}

// __restrict__ = promising the compiler that any data written to through that pointer is not read by any other pointer 
__global__
void saxpy_kernel(int n, float a, const float* __restrict__ x, float* __restrict__ y) {
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	if(i<n)
		y[i] = a*x[i] + y[i];
}

void saxpy(float a, const std::vector<float>& x, std::vector<float>& y) {
	
	int N = x.size();
	float* x_dvc;
	float* y_dvc;
	
	checkError( cudaMalloc(&x_dvc, sizeof(float)*N) );
	checkError( cudaMalloc(&y_dvc, sizeof(float)*N) );
	
	checkError( cudaMemcpy(x_dvc, x.data(), sizeof(float)*N, cudaMemcpyHostToDevice) );
	checkError( cudaMemcpy(y_dvc, y.data(), sizeof(float)*N, cudaMemcpyHostToDevice) );
	
	size_t blockSize = 256;
  size_t numBlocks = (N + blockSize - 1) / blockSize; // aufrunden falls N kein Mehrfaches von blockSize ist
	
	saxpy_kernel<<<numBlocks, blockSize>>>(N, a, x_dvc, y_dvc);
	checkError(cudaPeekAtLastError());
//  checkError(cudaDeviceSynchronize());

	checkError( cudaMemcpy(y.data(), y_dvc, sizeof(float)*N, cudaMemcpyDeviceToHost) );
	
	checkError( cudaFree(x_dvc) );
	checkError( cudaFree(y_dvc) );
}
