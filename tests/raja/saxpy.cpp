
#include "saxpy.h"
#include <iostream>
#include <RAJA/RAJA.hpp>

template<class T> void _unused( const T& ) { }
void unused() {}
template<typename Arg1, typename... Args>
void unused(const Arg1& arg1, const Args&... args)
{
    _unused( arg1 );
    unused(args...);
}


#if defined(POLICY_CUDA)
  using policy = RAJA::cuda_exec<256>;
  const std::string policy_name = "CUDA";
#elif defined(POLICY_OPENMP)
  using policy = RAJA::omp_parallel_for_exec;
  const std::string policy_name = "OpenMP";
#else
  using policy = RAJA::seq_exec;
  const std::string policy_name = "sequential";
#endif

#if defined(POLICY_CUDA)
	template <typename T>
	T* allocateAndCopy(const T* host_ptr, int N) {
		
		T* device_ptr;
		
		cudaErrchk( cudaMalloc(&device_ptr, sizeof(T)*N) );
		cudaErrchk( cudaMemcpy(device_ptr, host_ptr, sizeof(T)*N, cudaMemcpyHostToDevice) );
		
		return device_ptr;
	}
#else
	template <typename T>
	T* allocateAndCopy(T* host_ptr, int N) {
		unused(N);
		return host_ptr;
	}
	template <typename T>
	const T* allocateAndCopy(const T* host_ptr, int N) {
		unused(N);
		return host_ptr;
	}
#endif

template <typename T>
void copyToHost(T* host_ptr, const T* device_ptr, int N) {
	#if defined(POLICY_CUDA)
		cudaErrchk( cudaMemcpy(host_ptr, device_ptr, sizeof(T)*N, cudaMemcpyDeviceToHost) );
	#else
		unused(host_ptr, device_ptr, N);
	#endif
}

template <typename T>
void deviceFree(T* device_ptr) {
	#if defined(POLICY_CUDA)
		cudaErrchk( cudaFree(device_ptr) );
	#else
		unused(device_ptr);
	#endif
}

void saxpy(float a, const std::vector<float>& x, std::vector<float>& y) {
	
	int N = x.size();
	
	std::cout << "saxpy with N=" << N << " and policy: " << policy_name << '\n';
	
	auto x_dvc = allocateAndCopy(x.data(), N);
	auto y_dvc = allocateAndCopy(y.data(), N);
	
	RAJA::forall<policy>(RAJA::RangeSegment(0,N), [=] RAJA_DEVICE (int i) {
		y_dvc[i] = a*x_dvc[i] + y_dvc[i];
	});

	copyToHost(y.data(), y_dvc, N);
	
	deviceFree(x_dvc);
	deviceFree(y_dvc);
}
