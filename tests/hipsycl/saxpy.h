#include <vector>

// “Single-Precision A·X Plus Y”
// computes y = alpha * x + y
void saxpy(float alpha, const std::vector<float>& x, std::vector<float>& y);
