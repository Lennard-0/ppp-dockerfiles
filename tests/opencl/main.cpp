
#include "saxpy.h"
#include <iostream>

int main() {
	
	int N = 1<<28;
	
	std::vector<float> y(N, 1);
	std::vector<float> x(N, 2);
	
	saxpy(2,x,y);
	
	for(auto const& el : y)
		if(el != 5) {
			std::cerr << "failure: wrong result\n";
			return -1;
		}
	
	std::cout << "success!\n";
	
	return 0;
}
