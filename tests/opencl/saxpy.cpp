
#include "saxpy.h"
#include <iostream>
#include <filesystem>
#include <fstream>
#define __CL_ENABLE_EXCEPTIONS
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#include <CL/cl2.hpp>
#include <assert.h>

std::string readFileToString(const std::filesystem::path& filePath) {
	if(!std::filesystem::is_regular_file(filePath))
		throw std::runtime_error(std::string("error in ") + __PRETTY_FUNCTION__ + ": " + filePath.string() + " is not a regular file");
	std::fstream kernelFile(filePath);
	std::string content(
		(std::istreambuf_iterator<char>(kernelFile)),
		std::istreambuf_iterator<char>()
	);
	return content;
}

cl::Program createProgram(const cl::Context& context, const std::filesystem::path& kernelFilePath) {

	cl_int err = 0;
	cl::Program program(context, readFileToString(kernelFilePath), false, &err);
	
	if(err)
		throw std::runtime_error("OpenCL error: " + std::to_string(err) + " in " + __PRETTY_FUNCTION__ );

	try
	{
		program.build("-cl-std=CL1.2");
	}
	catch (cl::Error& e)
	{
		if (e.err() == CL_BUILD_PROGRAM_FAILURE) {
			for (cl::Device dev : program.getInfo<CL_PROGRAM_DEVICES>()) {
				// Check the build status
				cl_build_status status = program.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(dev);
				if (status != CL_BUILD_ERROR)
					continue;

				// Get the build log
				std::string name = dev.getInfo<CL_DEVICE_NAME>();
				std::string buildlog = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(dev);
				std::cerr << "Build log for " << name << ":" << std::endl
									<< buildlog << std::endl;
			}
			throw e;
		}
		else
		{
			std::cerr << "unknown cl::Error during build\n";
			throw e;
		}
	}
//    throw std::runtime_error("OpenCL error: " + std::to_string(err) + " in " + __PRETTY_FUNCTION__ );

	return program;
}

void saxpy(float a, const std::vector<float>& x, std::vector<float>& y) {
	
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	assert(!platforms.empty());

	std::cout << "known platforms:\n";
	for(auto const& platform : platforms) {
		std::cout << "  " << platform.getInfo<CL_PLATFORM_NAME>() << " with OpenCL version: " << platform.getInfo<CL_PLATFORM_VERSION>() << '\n';
		std::vector<cl::Device> devices;
		platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
		assert(!devices.empty());

		std::cout << "    with devices:\n";
		for(auto const& device : devices)
			std::cout << "      " << device.getInfo<CL_DEVICE_NAME>() << '\n';
	}
	auto platform = platforms.front();
	std::vector<cl::Device> devices;
	platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
	auto clDevice = devices.front();
	std::cout << "using device " << clDevice.getInfo<CL_DEVICE_NAME>()
						<< " from platform " << platform.getInfo<CL_PLATFORM_NAME>() << std::endl;

	auto clContext = cl::Context(clDevice);
	
	
	auto saxpyProgram = createProgram(clContext, "/tests/opencl/saxpy.cl");
	
	
	
	
	int N = x.size();
	
	cl::Buffer x_buf(clContext,
									 CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
									 sizeof(float) * N,
									 (void*) x.data());
	cl::Buffer y_buf(clContext,
									 CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR,
									 sizeof(float) * N,
									 (void*) y.data());
	
	
	cl::Kernel saxpyKernel(saxpyProgram, "saxpyKernel");
    saxpyKernel.setArg(0, a);
    saxpyKernel.setArg(1, x_buf);
    saxpyKernel.setArg(2, y_buf);
  
  cl::CommandQueue queue(clContext, clDevice);

	queue.enqueueNDRangeKernel(saxpyKernel, cl::NullRange, cl::NDRange(N));
	queue.enqueueReadBuffer(y_buf, CL_TRUE, 0, sizeof(float) * N, y.data());
	
}
