cmake_minimum_required(VERSION 3.18)
project(KokkosTest)

find_package(Kokkos REQUIRED)

add_executable(kokkos-test
								main.cpp saxpy.h saxpy.cpp)

target_link_libraries(kokkos-test PUBLIC Kokkos::kokkos)

if(USE_CUDA)
	target_compile_definitions(kokkos-test PUBLIC USE_CUDA)
	kokkos_check(OPTIONS CUDA_LAMBDA)
endif()
